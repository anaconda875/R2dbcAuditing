package com.example.demo.configuration;

import com.example.demo.DemoApplication;
import com.example.demo.repository.CustomR2dbcRepositoryFactoryBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Configuration
@EnableR2dbcRepositories(basePackageClasses = DemoApplication.class, repositoryFactoryBeanClass = CustomR2dbcRepositoryFactoryBean.class)
public class R2dbcRepositoryConfiguration {
}
