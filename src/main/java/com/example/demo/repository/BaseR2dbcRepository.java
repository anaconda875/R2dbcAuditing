package com.example.demo.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseR2dbcRepository<E, ID> extends BaseRepository, R2dbcRepository<E, ID> {
}
