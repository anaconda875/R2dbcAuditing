package com.example.demo.repository;

import com.example.demo.repository.impl.SimpleR2dbcRepositoryImpl;
import org.springframework.data.r2dbc.convert.R2dbcConverter;
import org.springframework.data.r2dbc.core.R2dbcEntityOperations;
import org.springframework.data.r2dbc.repository.support.R2dbcRepositoryFactory;
import org.springframework.data.r2dbc.repository.support.R2dbcRepositoryFactoryBean;
import org.springframework.data.relational.repository.query.RelationalEntityInformation;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import java.io.Serializable;

public class CustomR2dbcRepositoryFactoryBean<T extends Repository<S, ID>, S, ID extends Serializable> extends R2dbcRepositoryFactoryBean<T, S, ID> {

    /**
     * Creates a new {@link R2dbcRepositoryFactoryBean} for the given repository interface.
     *
     * @param repositoryInterface must not be {@literal null}.
     */
    public CustomR2dbcRepositoryFactoryBean(Class<? extends T> repositoryInterface) {
        super(repositoryInterface);
    }

    @Override
    protected RepositoryFactorySupport getFactoryInstance(R2dbcEntityOperations operations) {
        return new CustomR2dbcRepositoryFactory(operations);
    }

    static class CustomR2dbcRepositoryFactory extends R2dbcRepositoryFactory {

        private final R2dbcEntityOperations operations;
        private final R2dbcConverter converter;

        public CustomR2dbcRepositoryFactory(R2dbcEntityOperations operations) {
            super(operations);
            this.operations = operations;
            this.converter = operations.getConverter();
        }

        @Override
        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
            return SimpleR2dbcRepositoryImpl.class;
        }

        @Override
        protected Object getTargetRepository(RepositoryInformation information) {
            RelationalEntityInformation<?, ?> entityInformation = getEntityInformation(information.getDomainType());
            return getTargetRepositoryViaReflection(information, entityInformation, operations, this.converter);
        }

    }
}
