package com.example.demo.repository.impl;

import com.example.demo.repository.BaseR2dbcRepository;
import org.springframework.data.r2dbc.convert.R2dbcConverter;
import org.springframework.data.r2dbc.core.R2dbcEntityOperations;
import org.springframework.data.r2dbc.repository.support.SimpleR2dbcRepository;
import org.springframework.data.relational.repository.query.RelationalEntityInformation;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public class SimpleR2dbcRepositoryImpl<E, ID> extends SimpleR2dbcRepository<E, ID> implements BaseR2dbcRepository<E, ID> {

    public SimpleR2dbcRepositoryImpl(RelationalEntityInformation<E, ID> entity, R2dbcEntityOperations entityOperations, R2dbcConverter converter) {
        super(entity, entityOperations, converter);
    }

}
